#!/usr/bin/env bash

sudo apt-get update

sudo apt-get install -y git
sudo apt-get install -y nodejs

#####################
#       VIM
#####################
sudo apt-get install -y vim

sudo mkdir /home/vagrant/.vim
sudo mkdir /home/vagrant/.vim/syntax
sudo cp /vagrant/asciidoc.vim /home/vagrant/.vim/syntax/asciidoc.vim

sudo rm /usr/share/vim/vimrc
sudo cp /vagrant/vimrc /usr/share/vim/vimrc
sudo chmod 644 /usr/share/vim/vimrc

sudo mkdir /home/vagrant/.ssh
sudo cp /vagrant/.ssh/*  /home/vagrant/.ssh/

#####################
#      APACHE
#####################
sudo apt-get install -y apache2
sudo rm -rf /var/www
sudo ln -fs /vagrant /var/www
